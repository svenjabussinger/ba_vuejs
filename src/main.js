// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import {store} from './store'
import Vuex from 'vuex'
import VueChatScroll from 'vue-chat-scroll'

// Vuex und VueChatScroll in Vue verfügbar machen
Vue.use(Vuex)
Vue.use(VueChatScroll)

// Komponenten importieren und in gesamter Anwendung verfügbar machen
import Login from './components/Login'
Vue.component('baLogin', Login)
import Register from './components/Register'
Vue.component('baRegister', Register)
import Chatheader from './components/Chatheader'
Vue.component('baChatheader', Chatheader)
import Chat from './components/Chat'
Vue.component('baChat', Chat)
import Userinfo from './components/Userinfo'
Vue.component('baUserInfo', Userinfo)
import Message from './components/Message'
Vue.component('baMessage', Message)
import Messageinput from './components/Messageinput'
Vue.component('baMessageInput', Messageinput)

Vue.config.productionTip = false

// Neue Vue Instanz in das app Element der index.html mit App Komponente rendern
/* eslint-disable no-new */
new Vue({
  el: '#app',
  store,
  router,
  template: '<App/>',
  components: { App }
})
