// Vuex store modul user
import {db} from '../../firebase'
import * as types from '../mutation-types'
import * as firebase from 'firebase'
import router from '../../router'

// initial user state
const state = {
  user: null,
  currentUserData: null,
  userId: ''
}

// user getters
const getters = {
  getCurrentUser: state => state.user,
  getCurrentUserData: state => state.currentUserData,
  getUserId: state => state.userId
}

// user actions
const actions = {
  getUserAuth ({commit}) {
    commit(types.GET_USER_AUTH)
  },
  // Login User mit Firebase Authentication
  loginUser ({commit, dispatch}, {email, password}) {
    firebase.auth().signInWithEmailAndPassword(email, password)
      .then((user) => {
        // commit Vuex store mutations
        commit(types.SET_USER, {user})
        commit(types.SET_USER_ID, user.uid)
        // dispatch Vuex store action setUserData
        dispatch('setUserData', user.uid)
        // Route zu chat wechseln
        router.push('chat')
      })
      .catch((error) => {
        console.log(error)
      })
  },
  // neuen Nutzer registrieren mit Firebase Authentication
  registerUser ({dispatch}, {firstname, lastname, email, birthday, password}) {
    firebase.auth().createUserWithEmailAndPassword(email, password)
      .then((user) => {
        console.log('new user created with: ' + user.uid)
        // dispatch Vuex store action saveUserInfoFromForm
        dispatch('saveUserInfoFromForm', {uid: user.uid, firstname, lastname, birthday})
        // dispatch Vuex store action loginUser
        dispatch('loginUser', {email, password})
      })
      .catch((error) => {
        console.log(error)
      })
  },
  // zusätzliche Nutzerdaten bei Registrierung in Firebase Datenbank speichern --> führt keine Änderung am Store durch
  saveUserInfoFromForm ({commit}, {uid, firstname, lastname, birthday}) {
    db.ref().child('userData').child(uid).set({
      firstname: firstname,
      lastname: lastname,
      birthday: birthday
    })
      .then((user) => {
        console.log('Registered new User with data')
        console.log(user)
      })
      .catch((error) => {
        console.log(error)
      })
  },
  // Daten des aktuellen Nutzer aus Firebase Datenbank laden
  setUserData ({commit}, uid) {
    db.ref().child('userData').child(uid).on('value', user => {
      // commit Vuex store mutation
      commit(types.SET_USER_DATA, user.val())
    })
  },
  // User Logout mit Firebase Authentication
  logoutUser ({commit}) {
    firebase.auth().signOut()
      .then(() => {
        console.log('successfully logged out')
        // commit Vuex store mutation
        commit(types.USER_LOGOUT)
        // route zu logout wechseln
        router.push('logout', null)
      })
      .catch((error) => {
        console.log(error)
      })
  }
}

// user mutations
const mutations = {
  [types.GET_USER_AUTH] (state) {
    state.user = firebase.auth().currentUser
  },
  [types.USER_LOGOUT] (state, user) {
    state.user = user
    state.currentUserData = user
    state.userId = ''
  },
  [types.SET_USER] (state, {user}) {
    state.user = user
  },
  [types.SET_USER_DATA] (state, userData) {
    state.currentUserData = userData
  },
  [types.SET_USER_ID] (state, userId) {
    state.userId = userId
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
