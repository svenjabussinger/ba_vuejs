import * as types from '../mutation-types'
import {db} from '../../firebase'

// initial message state
const state = {
  messages: []
}

// message getters
const getters = {
  getMessages: state => state.messages
}

// message actions
const actions = {
  // neue Nachricht in Firebase Datenbank speichern
  saveNewMessage ({commit, dispatch}, {content, uid}) {
    let newMessage = {content, uid, timestamp: Date.now()}
    db.ref('messages').push(newMessage)
      .then(() => {
        // commit Vuex store mutation
        commit(types.SAVE_NEW_MESSAGE, {newMessage})
        // dispatch Vuex store action loadInitMessages
        dispatch('loadInitMessages')
      })
      .catch((error) => {
        console.log(error)
      })
  },
  // Nachrichten aus Firebase Datenbank laden
  loadInitMessages ({commit}) {
    db.ref('messages').on('value', snapshot => {
      let messages = snapshot.val()
      // commit Vuex store mutation
      commit(types.LOAD_INIT_MESSAGES, {messages})
    })
  }
}

// message mutations
const mutations = {
  [types.SAVE_NEW_MESSAGE] (state, {message}) {
    Object.assign({}, state, { messages: message })
  },
  [types.LOAD_INIT_MESSAGES] (state, {messages}) {
    state.messages = messages
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
