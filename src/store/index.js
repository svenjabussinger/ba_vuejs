// Vuex store
import Vue from 'vue'
import Vuex from 'vuex'
import * as actions from './actions'
import * as getters from './getters'
import user from './modules/user'
import message from './modules/message'

// Vuex in Vue verfügbar machen
Vue.use(Vuex)

// Vuex Store definieren mit user und message modules
export const store = new Vuex.Store({
  actions,
  getters,
  modules: {
    user,
    message
  }
})
