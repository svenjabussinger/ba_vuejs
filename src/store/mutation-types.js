// Mutation types

/* Message types  */
export const LOAD_INIT_MESSAGES = 'LOAD_INIT_MESSAGES'
export const SAVE_NEW_MESSAGE = 'SAVE_NEW_MESSAGE'

/* user types  */
export const GET_USER_AUTH = 'GET_USER_AUTH'
export const USER_LOGOUT = 'USER_LOGOUT'
export const SET_USER = 'SET_USER'
export const SET_USER_DATA = 'SET_USER_DATA'
export const SET_USER_ID = 'SET_USER_ID'
