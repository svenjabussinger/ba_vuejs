import * as firebase from 'firebase'
import {db} from '../firebase'
import router from '../router'

const fbAuth = firebase.auth()

export function getCurrentUser () {
  return fbAuth.currentUser
}

export function userLogin (email, password) {
  fbAuth.signInWithEmailAndPassword(email, password)
    .then((user) => {
      console.log('The user ' + user.uid + 'got logged in successfully')
      router.push('chat')
    })
    .catch((error) => {
      console.log(error)
    })
}

export function userRegister (firstname, lastname, email, birthday, password) {
  fbAuth.createUserWithEmailAndPassword(email, password)
    .then((user) => {
      console.log('User created with uid: ' + user.uid)
      saveUserInfoFromForm(user.uid, firstname, lastname, birthday)
      userLogin(email, password)
    })
    .catch((error) => {
      console.log(error)
    })
}

export function saveUserInfoFromForm (uid, firstname, lastname, birthday) {
  const userDataRef = db.ref().child('userData').child(uid)

  userDataRef.set({
    firstname: firstname,
    lastname: lastname,
    birthday: birthday
  })
    .then(() => {
      console.log('User Data got saved ')
    })
    .catch((error) => {
      console.log(error)
    })
}

export function userLogout () {
  fbAuth.signOut()
    .then(() => {
      console.log('user got logged out')
      // route to logout page
    })
    .catch((error) => {
      console.log(error)
    })
}

export function getUserData (uid) {
  db.ref().child('userData').child(uid).on('value', snapshot => {
    return snapshot.val()
  })
}
