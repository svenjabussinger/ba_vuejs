import {db} from '../firebase'

export function getMessages () {
  db.ref('messages').on('value', messages => {
    return messages.val()
  })
}

export function saveNewMessage (content, uid) {
  let messages = db.ref().child('messages')
  messages.push({
    content: content,
    uid: uid,
    timestamp: Date.now()
  })
}
