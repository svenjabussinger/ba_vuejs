// Konfiguration der Firebase Datenbank
import * as firebase from 'firebase'

const config = {
  apiKey: 'AIzaSyBuFdA9Vaezoqm5G5xQ4Ztq8kj05ZOmNzY',
  authDomain: 'chat-app-92b79.firebaseapp.com',
  databaseURL: 'https://chat-app-92b79.firebaseio.com',
  projectId: 'chat-app-92b79',
  storageBucket: 'chat-app-92b79.appspot.com',
  messagingSenderId: '297794186257'
}

export const firebaseApp = firebase.initializeApp(config)

export const db = firebaseApp.database()
