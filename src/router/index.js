// Router
import Vue from 'vue'
import Router from 'vue-router'
import LoginView from '@/views/LoginView'
import ChatView from '@/views/ChatView'
import LogoutView from '@/views/LogoutView'

// Router in Vue verfügbar machen
Vue.use(Router)

// Definition der Routen
export default new Router({
  routes: [
    {
      path: '/',
      redirect: '/chat'
    },
    {
      path: '/login',
      name: 'LoginView',
      component: LoginView
    },
    {
      path: '/chat',
      name: 'ChatView',
      component: ChatView
    },
    {
      path: '/logout',
      name: 'LogoutView',
      component: LogoutView
    }
  ]
})

